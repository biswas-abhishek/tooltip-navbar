// "use strict";
const gulp = require("gulp");
const cssnano = require("gulp-cssnano");
const autoprefixer = require("gulp-autoprefixer");

gulp.task("css-config", function () {
  return gulp
    .src("./public/css/style.css")
    .pipe(autoprefixer())
    .pipe(cssnano())
    .pipe(gulp.dest("./dist"));
});
