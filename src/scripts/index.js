"use strict";
const Btn = document.getElementById("btn");
const navbar_type = document.querySelector(".tooltip-navbar");
const vertical = document.querySelector(".vertical");
Btn.addEventListener("click", () => {
  navbar_type.classList.toggle("vertical");
  if (Btn.innerText === "vertical") {
    Btn.innerText = "horizontal";
  } else {
    Btn.innerText = "vertical";
  }
});
